use super::hash::{Hashable, H256};
use std::rc::Rc;

/// A Merkle tree.
#[derive(Debug, Default)]
pub struct MerkleTree {

    root : H256,

    left : Option<Box<MerkleTree>>,

    right : Option<Box<MerkleTree>>,

    numleaves : usize,
}

impl std::fmt::Display for MerkleTree {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "MerkleTree {{ root: {},  numleaves: {} }}", self.root, self.numleaves)
    }
}

impl MerkleTree {
    pub fn new<T>(data: &[T]) -> Self where T: Hashable, {
        let mut v = Vec::<&T>::new();
        for d in data.iter() {
            v.push(d);
        }
        // Copy the last item if an odd number
        if v.len() % 2 != 0 {
            v.push( &data[data.len() - 1] );
        }
        // create leaf nodes
        let mut nodes = Vec::<MerkleTree>::new();
        for n in v.iter() {
            let m = MerkleTree { root: n.hash(), left: None, right: None, numleaves: 1};
            nodes.push(m)
        }

        while nodes.len() > 1 {
            let mut next = Vec::<MerkleTree>::new();
            if (nodes.len()%2) == 1 {
                let m = MerkleTree { root: nodes[nodes.len()-1].root, left: None, right: None, numleaves: 0};
                nodes.insert(nodes.len(), m);
            }
            while !nodes.is_empty() {
                let left_node = nodes.remove(0);
                let right_node = nodes.remove(0);
                let left_leaves = left_node.numleaves;
                let right_leaves = right_node.numleaves;

                // Create a new hash for the concat of the two children nodes
                let mut ctx = ring::digest::Context::new(&ring::digest::SHA256);
                ctx.update( left_node.root.as_ref() );
                ctx.update( right_node.root.as_ref() );
                let new_hashable = H256::from(ctx.finish());

                // New parent node of left and right
                let new_node = MerkleTree{ 
                    root: new_hashable, 
                    left: Some(Box::new(left_node)), 
                    right : Some(Box::new(right_node)),
                    numleaves : left_leaves + right_leaves,
                };
                next.push(new_node);
            }
            nodes = next;
        }
        nodes.remove(0)
    }

    pub fn root(&self) -> H256 {
        self.root
    }

    /// Returns the Merkle Proof of data at index i
    pub fn proof(&self, index: usize) -> Vec<H256> {
        let mut left_idx : usize = 0;
        // Compute the virtual number of leaves: round up to closest power of 2
        let mut right_idx = powerof2(self.numleaves)-1;
        if index > right_idx { return Vec::<H256>::new() }

        let mut curr_root = self;
        let mut out = Vec::<H256>::new();
        
        loop {
            let mid_idx : usize = (right_idx + left_idx)/2; // Rounded down
            if index <= mid_idx {
                right_idx = mid_idx;
                match &curr_root.right {
                    Some(v) => out.insert( 0, v.root),
                    None => return out, 
                }
                match &curr_root.left {
                    Some(v) =>  curr_root = v,
                    None => panic!("qwe"),
                }
            } else if index > mid_idx {
                left_idx = mid_idx + 1;
                match &curr_root.left {
                    Some(v) => out.insert( 0, v.root ),
                    None => return out,
                }
                match &curr_root.right {
                    Some(v) => curr_root = v,
                    None => panic!("uio"),
                }
            }
        }
    }
}

pub fn powerof2(x: usize) -> usize {
    // Compute the virtual number of leaves: round up to closest power of 2
    let mut i : u32 = 0;
    let mut out : usize;
    loop {
        let new_pow = 2usize.pow(i);
        if new_pow >= x {
            out = new_pow;
            break;
        }
        i = i+1;
    }
    out

}

/// Verify that the datum hash with a vector of proofs will produce the Merkle root. Also need the
/// index of datum and `leaf_size`, the total number of leaves.
pub fn verify(root: &H256, datum: &H256, proof: &[H256], index: usize, leaf_size: usize) -> bool {
    let mut new_proof = proof.to_vec();
    let mut curr_index = index;
    let mut num_leaves = leaf_size;
   
    //// Compute the virtual number of leaves: round up to closest power of 2
    num_leaves = powerof2(leaf_size); 
    let mut running_hash = Vec::<H256>::new();
    println!("Proof: {:?}", proof);
    println!("Datum: {:?}", datum);
    running_hash.insert(0, *datum);
    while num_leaves > 1 {
        let mut ctx = ring::digest::Context::new(&ring::digest::SHA256);
        if (curr_index%2) == 1 {
            let first = new_proof.remove(0);
            let second = running_hash.remove(0);
            //ctx.update(new_proof.remove(0).as_ref());
            ctx.update(first.as_ref());
            //ctx.update(running_hash.remove(0).as_ref());
            ctx.update(second.as_ref());
            running_hash.insert(0, H256::from(ctx.finish()));
        } else {
            let first = running_hash.remove(0);
            let second = new_proof.remove(0);
            //ctx.update(running_hash.remove(0).as_ref());
            //ctx.update(new_proof.remove(0).as_ref());
            ctx.update(first.as_ref());
            ctx.update(second.as_ref());
            running_hash.insert(0, H256::from(ctx.finish()));
        }
        curr_index = curr_index/2;
        num_leaves = num_leaves/2;
    }
    root == &(running_hash.remove(0))
}

#[cfg(test)]
mod tests {
    use crate::crypto::hash::H256;
    use super::*;

    macro_rules! gen_merkle_tree_data {
        () => {{
            vec![
                (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
            ]
        }};
    }
    
    macro_rules! gen_merkle_tree_data2 {
        () => {{
            vec![
                (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
                (hex!("1234567890123456789012345678901234567890123456789012345678901234")).into(),
                (hex!("0987654321098765432109876543210987654321098765432109876543210987")).into(),
                (hex!("abcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcd")).into(),
            ]
        }};
    }

    #[test]
    fn root() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920")).into()
        );
        // "b69566be6e1720872f73651d1851a0eae0060a132cf0f64a0ffaea248de6cba0" is the hash of
        // "0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d"
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
        // "6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920" is the hash of
        // the concatenation of these two hashes "b69..." and "965..."
        // notice that the order of these two matters
    }

    #[test]
    fn proof() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert_eq!(proof,
                   vec![hex!("965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f").into()]
        );
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
    }

    #[test]
    fn verifying() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(1);
        assert!(verify(&merkle_tree.root(), &input_data[1].hash(), &proof, 1, input_data.len()));
    }
}
