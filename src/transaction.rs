use serde::{Serialize,Deserialize};
use ring::rand::{SystemRandom, generate};
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters, UnparsedPublicKey};
use crate::crypto::hash::{Hashable, H256};

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Transaction {
    input : u8,
    output : u8,
}

impl Hashable for Transaction {
    fn hash(&self) -> H256 {
        ring::digest::digest(&ring::digest::SHA256, bincode::serialize(&self).unwrap().as_slice()).into()
    }
}

/// Create digital signature of a transaction
pub fn sign(t: &Transaction, key: &Ed25519KeyPair) -> Signature {
    let sertx = bincode::serialize(&t).unwrap();
    key.sign( sertx.as_slice() )
}

/// Verify digital signature of a transaction, using public key instead of secret key
pub fn verify(t: &Transaction, public_key: &<Ed25519KeyPair as KeyPair>::PublicKey, signature: &Signature) -> bool {
    let sertx = bincode::serialize(&t).unwrap();
    let pkey = ring::signature::UnparsedPublicKey::new(&ring::signature::ED25519, public_key.as_ref());
    let result = pkey.verify(&sertx[..], signature.as_ref());
    match result {
        Ok(v) => true,
        Err(e) => false,
    }
}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::crypto::key_pair;

    pub fn generate_random_transaction() -> Transaction {
        let rng = SystemRandom::new();
        let rand_bytes : [u8; 4] = generate(&rng).unwrap().expose();
        Transaction {
            input: rand_bytes[0],
            output: rand_bytes[1],
        }
    }

    #[test]
    fn sign_verify() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        assert!(verify(&t, &(key.public_key()), &signature));
    }
}
